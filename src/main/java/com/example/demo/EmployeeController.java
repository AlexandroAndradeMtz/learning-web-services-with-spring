package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * this is our Employees REST API
 */
@RestController
@RequestMapping(path = "/employees")
public class EmployeeController {

    private Map<Long, Employee> employeesDatabase = new HashMap<>();

    @GetMapping("/find-by-id/{empId}")
    public Employee findById(@PathVariable long empId) {
        return employeesDatabase.get(empId);
    }

    @GetMapping("/find-by-name")
    public String findByName() {
        return "this is the find by name endpoint";
    }

    @PostMapping
    public Employee save(@RequestParam long empId, @RequestParam String name) {
        Employee newEmployee = new Employee(empId, name);
        employeesDatabase.put(newEmployee.empId, newEmployee);
        return newEmployee;
    }

    public record Employee(long empId, String name) { }

}
